## dpvs_exporter is a simple tool to export [DPVS](https://github.com/iqiyi/dpvs) stats to Prometheus

### Usage
```
Usage:
  dpvs_exporter [optional-params]
Simple tool for exporting DPVS stats to Prometheus
  Options(opt-arg sep :|=|spc):
  -h, --help                                print this help message
  -p=, --path=     string  "/usr/bin/dpip"  path to dpip binary
  -a=, --address=  string  ""               address to binds on
  --port=          int     9101             port, by default 9101
```



