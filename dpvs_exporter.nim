import strutils, tables, nre, asynchttpserver, asyncdispatch, os, osproc, strformat, cligen

type
  Stat = object
    ipackets: string
    opackets: string
    ibytes: string
    obytes: string
    ierrors: string
    oerrors: string

  Step = enum
    GET_IFACE,
    GET_STATS,
    GET_ERRORS

var DPIP_PATH: string


proc parse_output(text: string): TableRef[string, Stat] =
  let
    iface_re = re"\d+:\s+([a-z0-9]+[\.0-9]+)\:.*"
    stat_re = re"\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)"

  result = newTable[string, Stat]()
  var
    step = GET_IFACE
    matches: Option[RegexMatch]

  var
    iface = ""
    stat: Stat

  for line in text.split('\n'):
    case step
    of GET_IFACE:
      matches = line.find(iface_re)
      if matches.isSome:
        iface = matches.get().captures()[0]
        step = GET_STATS
    of GET_STATS:
      matches = line.find(stat_re)
      if matches.isSome:
        let caps = matches.get().captures()
        stat.ipackets = caps[0]
        stat.opackets = caps[1]
        stat.ibytes = caps[2]
        stat.obytes = caps[3]
        step = GET_ERRORS
    of GET_ERRORS:
      matches = line.find(stat_re)
      if matches.isSome:
        let caps = matches.get().captures()
        stat.ierrors = caps[0]
        stat.oerrors = caps[1]
        step = GET_IFACE
        result[iface] = stat

var server = newAsyncHttpServer()
proc cb(req: Request) {.async, gcsafe.} =
  if req.url.path == "/metrics":
    let
      output = execProcess(&"{DPIP_PATH} link show -s")
      stats = parse_output(output)

    var text = ""
    for p in stats.pairs():
      let
        device = p[0]
        stat = p[1]

      text.add &"""
node_network_receive_packets{{device="{device}"}} {stat.ipackets}
node_network_transmit_packets{{device="{device}"}} {stat.opackets}
node_network_receive_bytes{{device="{device}"}} {stat.ibytes}
node_network_transmit_bytes{{device="{device}"}} {stat.obytes}
node_network_receive_errs{{device="{device}"}} {stat.ierrors}
node_network_transmit_errs{{device="{device}"}} {stat.oerrors}
"""
    await req.respond(Http200, text)
  else:
    await req.respond(Http404, "<a href=\"/metrics\"/>/metrics</a>")

proc dpvs_exporter(path="/usr/bin/dpip", address="", port=9101): int =
  ## Simple tool for exporting DPVS stats to Prometheus
  if not path.fileExists:
    quit(&"Error: dpip not found: {path}")
  DPIP_PATH = path
  waitFor server.serve(Port(port), cb, address=address)
  return 1

when isMainModule:
  dispatch(dpvs_exporter, help = {
    "path": "path to dpip binary",
    "address": "address to binds on",
    "port": "port, by default 9101"
  })
